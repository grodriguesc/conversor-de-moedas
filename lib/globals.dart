library my_prj.globals;

import 'package:flutter/cupertino.dart';

class Moeda {
  String nome;
  String simbolo;
  double valor;
  TextEditingController moedaController;

  Moeda(this.nome, this.simbolo, this.valor, this.moedaController);
}

List<Moeda> moedas = List();
