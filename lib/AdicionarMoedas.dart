import 'package:flutter/material.dart';
import 'globals.dart' as globals;
import 'main.dart';

class AdicionarMoeda extends StatefulWidget {
  @override
  _AdicionarMoedaState createState() => _AdicionarMoedaState();
}

class _AdicionarMoedaState extends State<AdicionarMoeda> {
  globals.Moeda novaMoeda;

  final nomeController = TextEditingController();
  final simboloController = TextEditingController();
  final valorController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("\$ Conversor \$"),
          backgroundColor: Colors.blue,
          centerTitle: true,
        ),
        body: Container(
          child: Column(
            children: [
              TextField(
                controller: nomeController,
                decoration: InputDecoration(hintText: 'Nome'),
              ),
              Divider(),
              TextField(
                controller: simboloController,
                decoration: InputDecoration(hintText: 'Simbolo'),
              ),
              Divider(),
              TextField(
                controller: valorController,
                decoration: InputDecoration(hintText: 'Valor'),
              ),
              Divider(),
              FlatButton(
                color: Colors.blueAccent,
                onPressed: () {
                  globals.moedas.add(globals.Moeda(
                      nomeController.text,
                      simboloController.text,
                      double.parse(valorController.text),
                      TextEditingController()));
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Home(),
                      ));
                },
                child: Text(
                  "Adicionar nova moeda",
                  style: TextStyle(color: Colors.white),
                  textAlign: TextAlign.center,
                ),
              )
            ],
          ),
        ));
  }
}
