import 'package:conversor/AdicionarMoedas.dart';
import 'package:flutter/material.dart';
import 'AdicionarMoedas.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'globals.dart' as globals;

const request = "https://api.hgbrasil.com/finance";

void main() async {
  runApp(MaterialApp(
    home: Home(),
<<<<<<< HEAD
    batataDoce(),
=======
    BatataComSal(),
>>>>>>> master
    theme: ThemeData(hintColor: Colors.blue, primaryColor: Colors.white),
  ));
}

http.Response response;
Future<Map> getData() async {
  if (response == null) {
    response = await http.get(request);
  }
  return json.decode(response.body);
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final realController = TextEditingController();
  final dolarController = TextEditingController();
  final euroController = TextEditingController();

  double dolar;
  double euro;

  List<Widget> widgetMoeda = new List<Widget>();

  _valueChanged(String text) {
    if (text.isEmpty) {
      _clearAll();

      return;
    }
    var valor = double.parse(text);
    var cotacao;
    print(text);

    for (var i = 0; i < globals.moedas.length; i++) {
      if (globals.moedas[i].moedaController.text == text) {
        print('moeda ' + globals.moedas[i].moedaController.text);
        cotacao = globals.moedas[i].valor;
        print(cotacao);
      }
    }
    for (var i = 0; i < globals.moedas.length; i++) {
      if (globals.moedas[i].moedaController.text != text) {
        globals.moedas[i].moedaController.text =
            (valor * cotacao / globals.moedas[i].valor).toStringAsFixed(2);
      }
    }
    realController.text = (valor * cotacao).toStringAsFixed(2);
  }

  void _clearAll() {
    realController.text = "";
    for (var i = 0; i < globals.moedas.length; i++) {
      globals.moedas[i].moedaController.text = "";
    }
  }

  void _realChanged(String text) {
    if (text.isEmpty) {
      _clearAll();
      return;
    }
    double real = double.parse(text);

    for (var i = 0; i < globals.moedas.length; i++) {
      globals.moedas[i].moedaController.text =
          (real / globals.moedas[i].valor).toStringAsFixed(2);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text("\$ Conversor \$"),
          backgroundColor: Colors.blue,
          centerTitle: true,
        ),
        body: FutureBuilder<Map>(
            future: getData(),
            builder: (context, snapshot) {
              switch (snapshot.connectionState) {
                case ConnectionState.none:
                case ConnectionState.waiting:
                  return Center(
                      child: Text(
                    "Carregando Dados...",
                    style: TextStyle(color: Colors.blue, fontSize: 25.0),
                    textAlign: TextAlign.center,
                  ));
                default:
                  if (snapshot.hasError) {
                    return Center(
                        child: Text(
                      "Erro ao Carregar Dados :(",
                      style: TextStyle(color: Colors.blue, fontSize: 25.0),
                      textAlign: TextAlign.center,
                    ));
                  } else {
                    dolar =
                        snapshot.data["results"]["currencies"]["USD"]["buy"];
                    euro = snapshot.data["results"]["currencies"]["EUR"]["buy"];

                    if (globals.moedas.isEmpty) {
                      globals.moedas.add(globals.Moeda(
                          "Dólar", "US\$", dolar, TextEditingController()));
                      globals.moedas.add(globals.Moeda(
                          "Euro", "€", euro, TextEditingController()));
                    }
                    print(globals.moedas);
                    widgetMoeda.clear();
                    for (var i = 0; i < globals.moedas.length; i++) {
                      widgetMoeda.add(new Column(
                        children: [
                          buildTextField(
                              globals.moedas[i].nome,
                              globals.moedas[i].simbolo,
                              globals.moedas[i].moedaController,
                              _valueChanged),
                          Divider(),
                        ],
                      ));
                    }
                    return SingleChildScrollView(
                      padding: EdgeInsets.all(10.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Icon(Icons.monetization_on,
                              size: 150.0, color: Colors.lightBlue),
                          buildTextField(
                              "Reais", "R\$", realController, _realChanged),
                          Divider(),
                          Column(
                            children: widgetMoeda,
                          ),
                          FlatButton(
                            color: Colors.blueAccent,
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => AdicionarMoeda(),
                                  ));
                            },
                            child: Text(
                              "Adicionar nova moeda",
                              style: TextStyle(color: Colors.white),
                              textAlign: TextAlign.center,
                            ),
                          )
                        ],
                      ),
                    );
                  }
              }
            }));
  }
}

Widget buildTextField(
    String label, String prefix, TextEditingController c, Function f) {
  return TextField(
    controller: c,
    decoration: InputDecoration(
        labelText: label,
        labelStyle: TextStyle(color: Colors.lightBlue),
        border: OutlineInputBorder(),
        prefixText: prefix),
    style: TextStyle(color: Colors.blue, fontSize: 25.0),
    onChanged: f,
    keyboardType: TextInputType.numberWithOptions(decimal: true),
  );
}
